package com.neptune.jagintellij.cs2.gutter

import com.intellij.formatting.blocks.prev
import com.intellij.lang.ASTNode
import com.intellij.lang.folding.FoldingBuilderEx
import com.intellij.lang.folding.FoldingDescriptor
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.FoldingGroup
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.util.PsiTreeUtil
import com.neptune.jagintellij.cs2.ClientScriptParserDefinition.Companion.rules
import com.neptune.jagintellij.parser.ClientScriptParser

/**
 * The gutter area folding provider for the ClientScript language.
 *
 * @author Walied K. Yassen
 */
class ClientScriptLineFolderProvider : FoldingBuilderEx() {

    override fun buildFoldRegions(root: PsiElement, document: Document, quick: Boolean): Array<FoldingDescriptor> {
        val descriptors = arrayListOf<FoldingDescriptor>()
        PsiTreeUtil.collectElements(root) { isFoldable(it) }.forEach {
            descriptors.add(object : FoldingDescriptor(it.node, TextRange(it.textRange.startOffset, it.textRange.endOffset), FoldingGroup.newGroup(it.node.elementType.toString())) {
                override fun getPlaceholderText(): String? {
                    return getFoldableText(it)
                }
            })
        }
        return descriptors.toTypedArray()
    }

    override fun getPlaceholderText(node: ASTNode) = node.text

    override fun isCollapsedByDefault(node: ASTNode) = false

    /**
     * Checks whether or not the specified [element] can be folded.
     *
     * @return *true* if it is foldable otherwise *false*.
     */
    fun isFoldable(element: PsiElement) = when (element.node.elementType) {
        rules[ClientScriptParser.RULE_script],
        rules[ClientScriptParser.RULE_blockStatement] -> true
        else -> false
    }

    /**
     * Gets the foldable placeholder text of the specified [element].
     *
     * @return the foldable placeholder text of the element.
     */
    fun getFoldableText(element: PsiElement) = when (element.node.elementType) {
        rules[ClientScriptParser.RULE_script] -> element.node.firstChildNode.text
        rules[ClientScriptParser.RULE_blockStatement] -> "{ ... }"
        else -> ""
    }
}
